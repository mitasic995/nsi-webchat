

var socket = io(); //kreiramo konekciju 

function scrollToBottom(){
    //selector
    var messages = jQuery('#messages');
    var newMessage = messages.children("li:last-child") //poslednje dete tipa <li> odnosno zadnja poruka
    //heights
    var clientHeight = messages.prop('clientHeight');
    var scrollTop = messages.prop('scrollTop');
    var scrollHeight = messages.prop('scrollHeight');
    var newMessageHeight = newMessage.innerHeight();
    var lastMessageHeight = newMessage.prev().innerHeight(); //prev - previouse child
    if(clientHeight+scrollTop+newMessageHeight+lastMessageHeight >= scrollHeight)
    {
        messages.scrollTop(scrollHeight); 
    }

};
socket.on('connect',function() {   

//    var params = jQuery.deparam();
 //   sessionStorage.setItem('username',params.username);
//    params.username = sessionStorage.getItem('username');
var roomTag = jQuery('.chat__main').attr('tag');
console.log(roomTag);
var params={
    'username': sessionStorage.getItem('username'),
    'room': roomTag
};

    socket.emit('Join',params,function(err){
        if(err){
            alert(err);
            window.location.href = '/lobby'; //redirekt - preusmeracamo ga nazad na root straincu
        }else{
            console.log('no errors :D');
        }
    });



    socket.on('disconnect',()=>{
        // acknowledgment 
        console.log('disconected from server');
    });
});

document.getElementById("message-form").addEventListener('submit',function(e){
    e.preventDefault();

    var user = sessionStorage.getItem('username');
    var room = jQuery('.chat__main').attr('tag');
    socket.emit('createMessage',{
        from: user,
        room: room,
        text: document.getElementById("message-input").value
    },function (){
        //callback (ACK) koji se okida na serveru
        //izvrsava se kad dodje ACK sa servera >D
        document.getElementById("message-input").value = ""; //cistimo input
    });
});

socket.on('newMessage',function(msg){
    var formattedTime = moment(msg.createdAt).format('h:mm a');
    
var string = ` <li class=\"message\"><div class=\"message__title\">    <h4>${msg.from}</h4><span>${formattedTime}</span></div><div class=\"message__body\"> <p>${msg.text}</p></div>`
console.log(string);
var  template = jQuery('#message-template').html();
    var html2 = Handlebars.compile(template);
    console.log(html2)
    var context = {from:msg.from,text:msg.text,createdAt:formattedTime};
    html2(context);

    var html = Mustache.render(template,{
        text: msg.text,
        from: msg.from,
        createdAt: formattedTime
        });
    console.log(html)
    jQuery('#messages').append(string);

    
    scrollToBottom();


});

socket.on('newLocationMessage',function(msg){
    var list = jQuery('#messages');

    var formatedTime = moment(msg.createdAt).format("h:mm a");

    var template = jQuery('#location-message-template').html();
    var  html = Mustache.render(template,{
        from:msg.from,
        createdAt: formatedTime,
        url:msg.url
    });
    list.append(html);
    scrollToBottom();
   
});

socket.on('UpdateUserList',function(usersList){
    console.log('..update user list');
   
    console.log(usersList);
    
    var ol = jQuery('<ul></ul>');
    
    usersList.forEach(function(user){
       
        ol.append(jQuery('<li></li>').text(user.user)); /// posto je lista=lista imena
    });

    //u div #users stavljamo celu listu koju smo kreirali
    jQuery('#users').html(ol);
});