const path = require('path');   
const fs = require('fs');
const hbs = require('hbs');
const bodyParser  =require('body-parser');
const publicPath = path.join(__dirname,'./public');
const express = require('express');
const http = require('http');
const socketIO = require('socket.io')
const _ = require('lodash');
const mongoose = require('./mongoose.js');
const {ObjectID} = require('mongodb');

var {User} =require('./models/user');
var {Room} = require('./models/room');

var session = require('express-session');

const app = express();

const {isRealString} = require('./utils/validation');
var {generateMessage,generateLocationMessage} = require('./utils/message');


app.set('view engine','hbs');
//middleware
app.use(express.static(publicPath,{}));
app.use(bodyParser.urlencoded({extended:true}));
app.use(session({secret:'123131',saveUninitialized:true,resave:false,cookie: {secure:false,httpOnly:false } }));
//post
const port = process.env.port || 3000;

server = http.createServer(app);
io = socketIO(server);


io.on('connection',(socket)=>{ //event- ada se uspostavi konkcija sa klijentom
    console.log('new user conectiong'); 
    

    
    socket.on('createMessage',(msg,callback)=>{
//    console.log(socket.id);
        // user = users.getUser(socket.id);
        // if (user && isRealString(msg.text))
        //saljemo poruke samo u toj sobi 
        
        var poruka = generateMessage(msg.from,msg.text);
        
        Room.findOne({name:msg.room},(err,docs)=>{
         
         docs.addMessage(poruka);  
         
         
        });
        io.to(msg.room).emit('newMessage',poruka);

        
    
        // socket.emit('newMessage',{ //socket.emit salje samo jednoj konekciji (konektovanom korisniku)
        //     text: msg.text,
        //     from: msg.from,
        //     createdAt: new Date()
        // });
        //kada koristimo io.emit salje se na sve konekcije(BROADCAST)
            //     io.emit('newMessage',generateMessage(msg.from,msg.text));
        // socket.broadcast.emit('newMessage',{ //bcast svima osim onome ko salje
        //     from: msg.from,
        //     text: msg.text,
        //     createdAt: new Date().getTime( )
        // });
        callback('this is from server');
    });

    socket.on('Join',(params,callback)=>{

        if(!isRealString(params.username) || !isRealString(params.room) )
        {
            return callback("name and room name are REQUIRED"); //saljemo callback sa err argument :D
        }

       socket.join(params.room);

        
        //io.to(params.room).emit('UpdateUserList',(users.getUserList(params.room)));         //okidamo UpdateUserList  event -prosledjuemo listu usera u toj sobi 
        
        var poruka = generateMessage("Admin",`${params.username} joined!`)
        socket.broadcast.to(params.room).emit('newMessage',poruka);
 
        Room.findOne({name:params.room},(err,docs)=>{
            
            docs.messages.forEach(element => {

                socket.emit('newMessage',{from:element.from,text:element.text,createdAt:element.createdAt});
            });
            var room = new Room(docs[0]);
            
            room.addUser(params.room,params.username,socket.id)

        io.to(params.room).emit('UpdateUserList',(docs.users));

            socket.emit('newMessage',generateMessage("Admin",`Welcome to ${params.room} |CHAT APP`));
            docs.addMessage(poruka);         
            
           });
           
        callback();
        });

    socket.on('disconnect',()=>{
            console.log('client disconected from server');
            //nesto sa bazom
            //get userbysocketID
            // io.to('')
    });

       
});



app.get('/',(req,res)=>{
    
//     if (req.session.username)
//         res.redirect('/lobby');

//     //  res.send('<h1>Hello Express!</h1>');
    
//         // res.render('home.hbs',{
//         //     homePage: 'Home Page',
//         //     godina: new Date(  ).getFullYear(  ),
//         //     welcomeMsg: "Welcome from backend! :D"
//         // })
//         console.log(req.session.username);
//  //   res.redirect('/lobby');
//     console.log("test")
//     res.send();
    });
app.get('/login',(req,res)=>{
    if (req.session.username !== undefined)
        return res.redirect('/lobby');
    console.log("get-login")

    res.type('html');
    
    res.render('login',(err,html)=>{
       
        res.send(html);
    })
})
app.get('/me',(req,res)=>{
    console.log(req.session.xauth);
    var auth = res.header('x-auth');
    console.log(auth);
    res.redirect('/');
})
app.post('/login',(req,res)=>{

    console.log('post-login')
    var body = _.pick(req.body,['username','password']);
   
    User.findByCredentials(body.username,body.password).then((user)=>{
        user.generateAuthToken().then((token)=>{
            req.session.username = body.username;
            req.session.xauth = token;
            res.header("x-auth",token);
           
            res.redirect('/lobby');
        });
    },(err)=>{//reject promise-findByCre
        if(err)
            res.render('register.hbs',{message:"Molimo Vas da se registrujete!"});
        else
            res.render('login.hbs',{message:"Pogresna lozinka!"});
        }) 


    //res.redirect('/lobby');
});
app.get('/lobby',(req,res)=>{
     console.log('get-lobby')
     
    console.log(req.session.username);
    if (req.session.username===undefined)
        return res.redirect('/login');
   
   // var body = _.pack(req.body,[])
    res.render('lobby.hbs',{username:req.session.username}); 

})
// app.post('/lobby',(req,res)=>{
//     console.log('post-lobby')
//     var body = _.pick(req.body,['username','room']);
//     req.session.room  =body.room;
//     req.session.save((err)=>{

//     });
    
//     //stoop hre
 
//  })
 app.post('/chat',(req,res)=>{
    console.log('chat-rom')
    
    var body = _.pick(req.body,['room']);
    req.session.room  = body.room;
    var username = req.session.username;

    var  soba = Room.find({name:body.room},(err,docs)=>{

        if (docs.length==0 ){

            console.log('dodavanje!')
            var ROOM = new Room({
                name: body.room,

            });
         ROOM.save();
            
        }else{
           // console.log(docs)
            var room = new Room(docs[0]);
       //     console.log('chat room ulazak dodavanje ',username)
            //room.addUser(body.room,username);

        }
        
    });
    // console.log(soba)
    // if(soba!= null)
    // {
      

    // }else{
    //     console("!!!")
    // var ROOM = new Room({
    //         name: body.room,
    //         users:{user:username},
    //     });
    
    //  ROOM.save();

    // }   
    // ROOM.addUser(username);
    // ROOM.save().then((room)=>{
    //     console.log(room);
    //     console.log('uspesno dodavanje sobe u bazu!')
    // })

  // console.log(req.session);
 //   console.log(body);      


    req.session.save((err)=>{
       // res.sendFile(__dirname+'/public/chat.html' );
       res.render('chat.hbs',{roomname: req.session.room});
    })
 
  
 }); 
 
app.get('/register',(req,res)=>{
    if (req.session.username )
        return res.redirect('/lobby');
    else
        res.sendFile(__dirname+'/public/register.html');
}   ); 
app.post('/register',(req,res)=>{
    if (req.session.username )
        return res.redirect('/loby');
    var body = _.pick(req.body,['username','email','password']);

   User.findOne({username:body.username},(err,docs)=>{
    if (!docs){
        var user = new User(body);
        console.log('resolve-block')
        user.save().then((user)=>{
            return user.generateAuthToken();
        }).then((token)=>{
            req.session.username = user.username;
            res.header("x-auth",token);
        return res.status(200).redirect('/lobby');
        }); 
    }else{
        console.log('reject-block')
        return res.render('register.hbs',{message:"username ili email adresaa vec registrovana!"});
    }
})
});
server.listen(port,()=>{
    console.log('SERVER STARTOVAN NA PORTU '+port);
});

