//izdvojen middleware u poseban .js fajl

var {User} = require('./../models/user');

var authenticite = (req,res,next)=>{
   
    var token = req.header('x-auth');
    User.findByToken(token).then((user)=>{
        if(!user)
            return Promise.reject(); // ovo ce da preusmeri kOd dole u CATCH blok ;)

     req.user = user;
     req.token = token;   //stavljamo u req jer ce dole da u get('/users/me') da se obradi
     
     next();//omogucamo da kod nastavi dalje
    }).catch((e)=>{
        // 401-auth requred
        res.status(401).send();
        //nema next() zbog greske
    });

};

module.exports = {authenticite}