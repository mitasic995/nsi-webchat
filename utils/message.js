var moment = require('moment');

var  generateMessage = (from,text)=>{
    return {
        from: from,
        text , //jer su ista imena
        createdAt:  moment().valueOf()
    };
};
var generateLocationMessage = (from,lat,lng)=>{

    return {
        from: from,
        url: `http://google.com/maps?q=${lat},${lng}`,
        createdAt: moment().valueOf()
    };
};

module.exports = {generateMessage,generateLocationMessage};