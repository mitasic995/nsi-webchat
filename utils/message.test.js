var expect = require('expect');

var {generateMessage} = require('./message')
describe('generateMessage',()=>{
    it('should generate correct message object',()=>{

        var from = 'Jen';
        var text = 'Some message';
        var message = generateMessage(from,text);
        
        //make assurctions
        expect(typeof(message.createdAt)).toBe('number');
        // expect(message).toInclude({ // ovo jer imamo dva propertija iz istog objeka
        //     from: from,  //ocekujemo da se from poklapa sa gore nasim from-om
        //     text: text   //isto tamo i za text
        // });

        //prepravka jer ovaj toInclude nece da radi ?D
        expect(message).toMatchObject({from, text});
    });
});
describe('generateLocationMessage',()=>{
 it('should generate correct geolocation message',()=>{
    //test-case za lokaciju
    
 });
});