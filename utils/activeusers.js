
//
//

// ES6 classes


// class Person {

//     constructor (name,age){
//         this.name = name;
//         this.age = age;
//     }
//     getUserDescription(){
//         return `${this.name} is ${this.age} year(s) old.`
//     }
// }

// var me =new Person('Milos',23);

class ActiveUsers{
    //ima propertz users[]
    constructor (){
        this.users =[];
    }
    addUser(id,name,room){
        //return added
        var user = {id,name,room};
        this.users.push(user);
        return user;
    }
    removeUser(id){
        //retrun removed user
        var user = this.getUser(id);
        if(user){
            this.users = this.users.filter((u)=> u.id !== id);
        }
        return user;
    }
    getUser(id){
        return this.users.filter((user)=> user.id === id)[0];
    }
   
    getUserList(room){
        var users = this.users.filter((user)=>{
            return user.room === room; //kriterijum za filter
        });
        var namesArray = users.map((user)=>{
            return user.name;
        });

        return namesArray;
    }
}

module.exports = {ActiveUsers};