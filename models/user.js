var mongoose = require('mongoose');
const validator = require('validator');
const jwt = require('jsonwebtoken');
const _ = require('lodash');
const bcrypt = require('bcryptjs');

var UserSchema = new mongoose.Schema({
   
    username: {
        type: String,
        required: true,
        trim: true,
        minlength: 3,
        unique: true,
        //validate
    },
    password:{
        type: String,
        require: true,
        minlength: 3,
    },
    email:{
        type: String,
        required: true,
        trim: true,
        minlength: 3,
        unique: true,
        validator:{
            validator:(value) =>{
                return validator.isEmail(value);
            },
            message: '{VALUE} nije validan emejl'
        }
    }
    ,
    tokens:[{
        access: {
            type: String,
            require: true
        },
        token:{
            type: String,
            require: true,
        }
    }],

});

UserSchema.methods.toJSON = function (){
 var user = this;
 var userObject = user.toObject();
 return _.pick(userObject,['_id','username','email']);
}

UserSchema.methods.generateAuthToken = function(){
    var user = this; // 
    var access = 'auth';
    var token = jwt.sign({_id: user._id.toHexString(), access: access},'abc123').toString(); //abc123=salting

    user.tokens.push({ access: access, token:token}); 
    
   return user.save().then(()=>{   
        return token;              
    });
};
    UserSchema.methods.removeToken = function(token){
    //mongodb operator $pull (izvlaci iz niza po odredjenom kriterijumu)
    var user =this;
    return user.update({  
        $pull: {
            tokens:{token}
        }
    });
};
UserSchema.statics.findByToken = function (token){
    var User = this;
    var decoded =undefined;

    try{//mora try-catch zbog verify
        decoded = jwt.verify(token,'abc123');

    }catch(e){
            //ako dodje do greske vracamo PROMISE , da bi mogli u server.js da obradimo gresku
            return new Promise((resolve,reject)=>{
                reject();
            });
            //moze i 
            // retrun Promise.reject();
    }
    return User.findOne({
        _id: decoded._id,
        'tokens.token': token, // 
        'tokens.access':  'auth'
    });//vracamo USER-a
};
    UserSchema.statics.findByCredentials = function(username,password){
        var user = this;
        return user.findOne({username:username}).then((user)=>{
            if (!user)
                return new Promise((resolve,reject)=>{ reject('nije registrovan!'); });
            
            return new Promise((resolve,reject)=>{
                bcrypt.compare(password,user.password,(err,result)=>{
                    if (result){
                        resolve(user);
                    }else {
                        reject();
                    }
                }) 
            })
        });
    };


//mongoose middleware
UserSchema.pre('save',function(next){
    var user = this;
    if(user.isModified('password')){ 
        bcrypt.genSalt(10,(err,salt)=>{         // 10 runde
            bcrypt.hash(user.password,salt,(err,hash)=>{
                user.password =  hash;
                next();
            });
        })
    }else{
        next();
    }
});
    var User = mongoose.model('User',UserSchema);
    module.exports = {User};