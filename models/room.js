var mongoose = require('mongoose');
const validator = require('validator');
const jwt = require('jsonwebtoken');
const _ = require('lodash');


var RoomSchema = new mongoose.Schema({

    name:{
        type: String,
        required: true,
        trim:true,
        minlength: 2,
        unique:true
    },
    users: [{
        user: {
            type: String,
            require:true,
            trim: true,
        },
        soketID: {
            type: String,
            trim:true
        }
    }],
    messages:[
        {
            from : {
                type:String,
                trim:true,
                require:true
            },
            text:{
                type: String,
                trim:true,
                require:true
            },
            createdAt:{
                type: Number,
                require:true,
            }
        }
    ]
    
});

RoomSchema.methods.toJSON = function (){
    var room = this;
    var roomObject = room.toObject();
    return _.pick(roomObject,['_id','name','users','messages']);
   };
RoomSchema.methods.getUsers = function(){
    return this.users;
}
RoomSchema.methods.addUser = function(roomname,user,soket){
    
    var room = this;
    console.log('room add meth room.users - ', room.getUsers())
    var pronadjen =room.users.filter((u)=>{u.user === user})
    if(pronadjen)
      room.users.push({user: user});

    return room.save().then(()=>{

    });     
};
RoomSchema.methods.addMessage = function(msg){
    var room = this;
    
    room.messages.push(msg)

     return room.save().then(()=>{

    });
};

RoomSchema.methods.removeUser = function(user){
    var room=this;
    room.users.update(
        {
        $pull:{user:user}
        }
    );
     return room.save().then(()=>{

    });
};

RoomSchema.statics.getByName = function(name){
    var  Room = this;
    return Room.findOne({name:name});
   
};
 var Room = mongoose.model('Room',RoomSchema);
module.exports  = {Room};